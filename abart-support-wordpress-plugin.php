<?php
/*
Plugin Name: Abart Support
Plugin URI: https://bitbucket.org/abartdigital/abart-support-wordpress-plugin
Description: Adds Support from Abart Digital to the website.
Author: Abart Digital
Version: 0.5
BitBucket Plugin URI: https://bitbucket.org/abartdigital/abart-support-wordpress-plugin
Text Domain: abart-wordpress-plugin
*/

add_action('admin_menu', 'abart_support_menu');

function abart_support_menu(){
    add_menu_page( 'Abart Support für Ihre Wordpress Website', 'Abart Support', 'edit_posts', 'abart-support', 'abart_support_content', 'dashicons-sos', '2' );
}

function abart_support_content(){
    echo '<h1>Abart Support</h1>';
    echo '<p>';
    echo __('Please use our support chat in the lower right corner, until we provide more information here.', 'abart-wordpress-support');
    echo '</p><br>';
    echo '<a href="https://lms.abart.dev/courses/wordpress-basics/" target="_blank" style="line-height: 1; text-decoration: none; dislay: inline-block; padding: 8px 16px; margin-right: 8px; margin-bottom: 8px; background-color: #111311; color: #fff; font-weight: bold;">' . __('WordPress Basics Course', 'abart-wordpress-support') . '</a>';
    echo '<br>';
    echo '<script src="//code.tidio.co/il80s8beoltsdqdolhdiolsx1wsihtzg.js" async></script>';
}

function abart_support_chat_user() {

}
add_action('admin_header', 'abart_support_chat_user');

function abart_support_chat() {
    $site = get_bloginfo();
    $current_user = wp_get_current_user();
    $current_user_id = $current_user->user_login;
    $current_user_email = $current_user->user_email;
    $current_user_firstname = $current_user->user_firstname;
    $current_user_lastname = $current_user->user_lastname;
    echo '<script type="text/javascript">
    document.tidioIdentify = {
      email: "' . $current_user_email .'",
      name: "' . $current_user_firstname . ' ' . $current_user_lastname .'"
    };
    tidioChatApi.addVisitorTags(["wordpress", "' . $site . '"]);
    tidioChatApi.setContactProperties({
        website: "' . $site . '"
      });
    </script>';
}
add_action('admin_footer', 'abart_support_chat');


/* Updates via Bitbucket */
require('vendor/autoload.php');

$repo = 'abartdigital/abart-support-wordpress-plugin';    // name of your repository. This is either "<user>/<repo>" or "<team>/<repo>".
$bitbucket_username = 'widmanno';                         // your personal BitBucket username
$bitbucket_app_pass = 'M7vwyhQfNMZnVCRCjnRz';             // the generated app password with read access

new \Maneuver\BitbucketWpUpdater\PluginUpdater(__FILE__, $repo, $bitbucket_username, $bitbucket_app_pass);
